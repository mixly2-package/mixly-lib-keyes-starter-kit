import * as Blockly from 'blockly/core';

//////////////////颜色/////////////////////
const KIT_HUE = 120;


/********************************************
           //Arduino基础学习套件//
                //数字输出//
*********************************************/

/********************************************
                  *LED灯*
*********************************************/
export const basic_led1 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_LED1)
            .appendField(new Blockly.FieldImage(require("../media/basic_led1.png"), 43, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
export const basic_led2 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_LED2)
            .appendField(new Blockly.FieldImage(require("../media/basic_led2.png"), 43, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
export const basic_led3 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_LED3)
            .appendField(new Blockly.FieldImage(require("../media/basic_led3.png"), 43, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};

export const basic_led4 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_LED4)
            .appendField(new Blockly.FieldImage(require("../media/basic_led4.png"), 43, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
/********************************************
                 *PWM输出*
*********************************************/
export const basic_a_Write = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_analog)
            .appendField(new Blockly.FieldImage(require("../media/basic_pwm.png"), 60, 32))
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendValueInput("num", Number)
            .appendField(Blockly.Msg.MIXLY_VALUE2)
            .setCheck(Number);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setInputsInline(true);
        this.setTooltip(Blockly.Msg.MIXLY_TOOLTIP_INOUT_ANALOG_WRITE);
    }
};

/********************************************
                  *rgb LED*
*********************************************/


export const basic_rgb01 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField("RGB")
            .appendField(new Blockly.FieldImage(require("../media/basic_rgb.png"), 43, 32));
        this.appendValueInput("R", Number)
            .appendField("R")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT);
        this.appendValueInput("r", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("R(0~255):");
        this.appendValueInput("G", Number)
            .appendField("G")
            .setAlign(Blockly.ALIGN_RIGHT)
            .setCheck(Number);
        this.appendValueInput("g", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("G(0~255):");
        this.appendValueInput("B", Number)
            .appendField("B")
            .setAlign(Blockly.ALIGN_RIGHT)
            .setCheck(Number);
        this.appendValueInput("b", Number)
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("B(0~255):");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};

/********************************************
                  *有源蜂鸣器*
*********************************************/
export const basic_y_buzzer = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_BUZZER1)
            .appendField(new Blockly.FieldImage(require("../media/basic_buzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};

/********************************************
                  *无源蜂鸣器*
*********************************************/
export const basic_w_buzzer1 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_BUZZER2)
            .appendField(new Blockly.FieldImage(require("../media/basic_pbuzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
//////////////////蜂鸣器//////////////////
var TONE_NOTES = [
    ["NOTE_C3", "131"], ["NOTE_D3", "147"], ["NOTE_E3", "165"], ["NOTE_F3", "175"], ["NOTE_G3", "196"], ["NOTE_A3", "220"], ["NOTE_B3", "247"],
    ["NOTE_C4", "262"], ["NOTE_D4", "294"], ["NOTE_E4", "330"], ["NOTE_F4", "349"], ["NOTE_G4", "392"], ["NOTE_A4", "440"], ["NOTE_B4", "494"],
    ["NOTE_C5", "532"], ["NOTE_D5", "587"], ["NOTE_E5", "659"], ["NOTE_F5", "698"], ["NOTE_G5", "784"], ["NOTE_A5", "880"], ["NOTE_B5", "988"]
];


export const basic_tone = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(new Blockly.FieldDropdown(TONE_NOTES), 'STAT');
        this.setOutput(true, Number);
    }
};

export const basic_w_buzzer2 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_BUZZER2)
            .appendField(new Blockly.FieldImage(require("../media/basic_pbuzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendValueInput('FREQUENCY')
            .setCheck(Number)
            //.setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_FREQUENCY);
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};
export const basic_w_buzzer3 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_BUZZER2)
            .appendField(new Blockly.FieldImage(require("../media/basic_pbuzzer.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendValueInput('FREQUENCY')
            .setCheck(Number)
            //.setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_FREQUENCY);
        this.appendValueInput('DURATION')
            .setCheck(Number)
            //.setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_DURATION);
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
    }
};

/********************************************
                  *电机*
*********************************************/
export const basic_motor = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_MOTOR)
            .appendField(new Blockly.FieldImage(require("../media/basic_motor.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_STAT)
            .appendField(new Blockly.FieldDropdown([[Blockly.Msg.MIXLY_ON, "HIGH"], [Blockly.Msg.MIXLY_OFF, "LOW"]]), "STAT");
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
/********************************************
                  数字传感器
                  *火焰传感*
*********************************************/
export const basic_flame = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_FLAME)
            .appendField(new Blockly.FieldImage(require("../media/basic_fire.png"), 50, 40));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setOutput(true, Boolean);
        this.setInputsInline(true);
        this.setTooltip('');
    }
};

/********************************************
                  *按键开关*
*********************************************/
export const basic_button = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_BUTTON)
            .appendField(new Blockly.FieldImage(require("../media/basic_button.png"), 43, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setOutput(true, Boolean);
        this.setInputsInline(true);
        this.setTooltip('');
    }
};

/********************************************
                  *倾斜开关*
*********************************************/
export const basic_tilt = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_TILT)
            .appendField(new Blockly.FieldImage(require("../media/basic_qingxie.png"), 43, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setOutput(true, Boolean);
        this.setInputsInline(true);
        this.setTooltip('');
    }
};


/********************************************
                  模拟传感器
                  *模拟温度*
*********************************************/
export const basic_analog_t = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_ANALOG_T)
            .appendField(new Blockly.FieldImage(require("../media/basic_temp.png"), 39, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setInputsInline(true);
        this.setOutput(true, Number);
        this.setTooltip('');
    }
};


/********************************************
                  *光线传感*
*********************************************/
export const basic_light = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_LIGHT)
            .appendField(new Blockly.FieldImage(require("../media/basic_guangmin.png"), 37, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setInputsInline(true);
        this.setOutput(true, Number);
        this.setTooltip('');
    }
};


/********************************************
                  *电位器*
*********************************************/
export const basic_potentiometer = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_POTENTIOMETER)
            .appendField(new Blockly.FieldImage(require("../media/basic_dianweiqi.png"), 37, 32));
        this.appendValueInput("PIN", Number)
            .appendField(Blockly.Msg.MIXLY_PIN)
            .setCheck(Number);
        this.setInputsInline(true);
        this.setOutput(true, Number);
        this.setTooltip('');
    }
};
/********************************************
                 显示屏
              *1位数码管*
*********************************************/
export const basic_seg1 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_SMG)
            .appendField(new Blockly.FieldImage(require("../media/basic_1seg.png"), 37, 32));

        this.appendValueInput("num")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_basic_value);

        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('');
    }
};

/********************************************
                  *4位数码管*
*********************************************/
export const basic_seg4 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_TM1637)
            .appendField(new Blockly.FieldImage(require("../media/basic_4seg.png"), 37, 32));

        this.appendValueInput("num")
            .setCheck(Number)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_basic_value);

        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setTooltip('');
    }
};



/********************************************
                  *1602LCD*
*********************************************/
export const basic_1602lcd = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField("LCD1602")
            .appendField(new Blockly.FieldImage(require("../media/basic_lcd1602.png"), 70, 32));
        this.appendValueInput("TEXT1", String)
            .setCheck([String, Number])
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_LCD_PRINT1);
        this.appendValueInput("TEXT2", String)
            .setCheck([String, Number])
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_LCD_PRINT2)
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};


/********************************************
                  *8*8点阵*
*********************************************/
export const basic_matrix_init = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_Matrix_init)
            .appendField(new Blockly.FieldImage(require("../media/basic_matrix.png"), 40, 40));
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};
//执行器_点阵屏显示_显示图案
export const basic_matrix1 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("").appendField(Blockly.Msg.MIXLY_basic_Matrix_custom).appendField(new Blockly.FieldImage(require("../media/basic_matrix.png"), 40, 40));
        this.appendValueInput("LEDArray").setAlign(Blockly.ALIGN_RIGHT).appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_PICARRAY);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        //this.setTooltip();
    }
};
//执行器_点阵屏显示_图案数组
export const basic_matrix2 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("").appendField(Blockly.Msg.MIXLY_DISPLAY_MATRIX_ARRAYVAR).appendField(new Blockly.FieldTextInput("LedArray1"), "VAR");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a81").appendField(new Blockly.FieldCheckbox("FALSE"), "a82").appendField(new Blockly.FieldCheckbox("FALSE"), "a83").appendField(new Blockly.FieldCheckbox("FALSE"), "a84").appendField(new Blockly.FieldCheckbox("FALSE"), "a85").appendField(new Blockly.FieldCheckbox("FALSE"), "a86").appendField(new Blockly.FieldCheckbox("FALSE"), "a87").appendField(new Blockly.FieldCheckbox("FALSE"), "a88");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a71").appendField(new Blockly.FieldCheckbox("FALSE"), "a72").appendField(new Blockly.FieldCheckbox("FALSE"), "a73").appendField(new Blockly.FieldCheckbox("FALSE"), "a74").appendField(new Blockly.FieldCheckbox("FALSE"), "a75").appendField(new Blockly.FieldCheckbox("FALSE"), "a76").appendField(new Blockly.FieldCheckbox("FALSE"), "a77").appendField(new Blockly.FieldCheckbox("FALSE"), "a78");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a61").appendField(new Blockly.FieldCheckbox("FALSE"), "a62").appendField(new Blockly.FieldCheckbox("FALSE"), "a63").appendField(new Blockly.FieldCheckbox("FALSE"), "a64").appendField(new Blockly.FieldCheckbox("FALSE"), "a65").appendField(new Blockly.FieldCheckbox("FALSE"), "a66").appendField(new Blockly.FieldCheckbox("FALSE"), "a67").appendField(new Blockly.FieldCheckbox("FALSE"), "a68");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a51").appendField(new Blockly.FieldCheckbox("FALSE"), "a52").appendField(new Blockly.FieldCheckbox("FALSE"), "a53").appendField(new Blockly.FieldCheckbox("FALSE"), "a54").appendField(new Blockly.FieldCheckbox("FALSE"), "a55").appendField(new Blockly.FieldCheckbox("FALSE"), "a56").appendField(new Blockly.FieldCheckbox("FALSE"), "a57").appendField(new Blockly.FieldCheckbox("FALSE"), "a58");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a41").appendField(new Blockly.FieldCheckbox("FALSE"), "a42").appendField(new Blockly.FieldCheckbox("FALSE"), "a43").appendField(new Blockly.FieldCheckbox("FALSE"), "a44").appendField(new Blockly.FieldCheckbox("FALSE"), "a45").appendField(new Blockly.FieldCheckbox("FALSE"), "a46").appendField(new Blockly.FieldCheckbox("FALSE"), "a47").appendField(new Blockly.FieldCheckbox("FALSE"), "a48");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a31").appendField(new Blockly.FieldCheckbox("FALSE"), "a32").appendField(new Blockly.FieldCheckbox("FALSE"), "a33").appendField(new Blockly.FieldCheckbox("FALSE"), "a34").appendField(new Blockly.FieldCheckbox("FALSE"), "a35").appendField(new Blockly.FieldCheckbox("FALSE"), "a36").appendField(new Blockly.FieldCheckbox("FALSE"), "a37").appendField(new Blockly.FieldCheckbox("FALSE"), "a38");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a21").appendField(new Blockly.FieldCheckbox("FALSE"), "a22").appendField(new Blockly.FieldCheckbox("FALSE"), "a23").appendField(new Blockly.FieldCheckbox("FALSE"), "a24").appendField(new Blockly.FieldCheckbox("FALSE"), "a25").appendField(new Blockly.FieldCheckbox("FALSE"), "a26").appendField(new Blockly.FieldCheckbox("FALSE"), "a27").appendField(new Blockly.FieldCheckbox("FALSE"), "a28");
        this.appendDummyInput("").appendField(new Blockly.FieldCheckbox("FALSE"), "a11").appendField(new Blockly.FieldCheckbox("FALSE"), "a12").appendField(new Blockly.FieldCheckbox("FALSE"), "a13").appendField(new Blockly.FieldCheckbox("FALSE"), "a14").appendField(new Blockly.FieldCheckbox("FALSE"), "a15").appendField(new Blockly.FieldCheckbox("FALSE"), "a16").appendField(new Blockly.FieldCheckbox("FALSE"), "a17").appendField(new Blockly.FieldCheckbox("FALSE"), "a18");
        this.setOutput(true, Number);
        //this.setTooltip();
    }
};

export const basic_matrix8 = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField(Blockly.Msg.MIXLY_basic_Matrix_char)
            .appendField(new Blockly.FieldImage(require("../media/basic_matrix.png"), 40, 40));
        this.appendValueInput("num")
            .setCheck([String, Number])
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(Blockly.Msg.MIXLY_basic_Matrix_dis);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};

export const basic_matrix_clear = {
    init: function () {
        this.setColour(KIT_HUE);
        this.appendDummyInput("")
            .appendField("matrix8*8 清屏")
            .appendField(new Blockly.FieldImage(require("../media/basic_matrix.png"), 40, 40));
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
    }
};