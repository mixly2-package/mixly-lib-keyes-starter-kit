//////////////////LED灯////////////////
export const basic_led1 = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dropdown_stat = this.getFieldValue('STAT');
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    var code = 'digitalWrite(' + dropdown_pin + ',' + dropdown_stat + ');\n'
    return code;
};

export const basic_led2 = basic_led1;
export const basic_led3 = basic_led1;
export const basic_led4 = basic_led1;

export const basic_a_Write = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var value_num = generator.valueToCode(this, 'NUM', generator.ORDER_ATOMIC);
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    var code = 'analogWrite(' + dropdown_pin + ',' + value_num + ');\n'
    return code;
};
//////////////////////////RGB//////////////////////////////
export const basic_rgb01 = function (_, generator) {
    var dropdown_pin1 = generator.valueToCode(this, 'R', generator.ORDER_ATOMIC);
    var value_r = generator.valueToCode(this, 'r', generator.ORDER_ATOMIC);

    var dropdown_pin2 = generator.valueToCode(this, 'G', generator.ORDER_ATOMIC);
    var value_g = generator.valueToCode(this, 'g', generator.ORDER_ATOMIC);

    var dropdown_pin3 = generator.valueToCode(this, 'B', generator.ORDER_ATOMIC);
    var value_b = generator.valueToCode(this, 'b', generator.ORDER_ATOMIC);

    generator.setups_['setup_output_' + dropdown_pin1] = 'pinMode(' + dropdown_pin1 + ', OUTPUT);';
    generator.setups_['setup_output_' + dropdown_pin2] = 'pinMode(' + dropdown_pin2 + ', OUTPUT);';
    generator.setups_['setup_output_' + dropdown_pin3] = 'pinMode(' + dropdown_pin3 + ', OUTPUT);';

    var code = 'analogWrite(' + dropdown_pin1 + ',' + value_r + ');\nanalogWrite(' + dropdown_pin2 + ',' + value_g + ');\nanalogWrite(' + dropdown_pin3 + ',' + value_b + ');\n';
    return code;
};

//////////////////////////有源蜂鸣器//////////////////////////////

export const basic_y_buzzer = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dropdown_stat = this.getFieldValue('STAT');
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    var code = 'digitalWrite(' + dropdown_pin + ',' + dropdown_stat + ');\n'
    return code;
};
//////////////////////////无源蜂鸣器//////////////////////////////
export const basic_w_buzzer1 = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dropdown_stat = this.getFieldValue('STAT');
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    var code = 'digitalWrite(' + dropdown_pin + ',' + dropdown_stat + ');\n'
    return code;
};

export const basic_w_buzzer2 = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var fre = generator.valueToCode(this, 'FREQUENCY',
        generator.ORDER_ASSIGNMENT) || '0';
    var code = "tone(" + dropdown_pin + "," + fre + ");\n";
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    return code;
};

export const basic_w_buzzer3 = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var fre = generator.valueToCode(this, 'FREQUENCY',
        generator.ORDER_ASSIGNMENT) || '0';
    var dur = generator.valueToCode(this, 'DURATION',
        generator.ORDER_ASSIGNMENT) || '0';
    var code = "tone(" + dropdown_pin + "," + fre + "," + dur + ");\n";
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    return code;
};

//////////////////////////电机//////////////////////////////
export const basic_motor = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    var dropdown_stat = this.getFieldValue('STAT');
    generator.setups_['setup_output_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', OUTPUT);';
    var code = 'digitalWrite(' + dropdown_pin + ',' + dropdown_stat + ');\n'
    return code;
};

//////////////////////////数字传感器////////////////////////////////

export const basic_button = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.setups_['setup_input_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', INPUT);';
    var code = 'digitalRead(' + dropdown_pin + ')';
    return [code, generator.ORDER_ATOMIC];
};
/////////////////火焰传感器////////////////
export const basic_flame = basic_button;

/////////////////倾斜传感器////////////////
export const basic_tilt = basic_button;


//////////////////////模拟传感器/////////////////////////

export const basic_light = function (_, generator) {
    var dropdown_pin = generator.valueToCode(this, 'PIN', generator.ORDER_ATOMIC);
    generator.setups_['setup_input_' + dropdown_pin] = 'pinMode(' + dropdown_pin + ', INPUT);';
    var code = 'analogRead(' + dropdown_pin + ')';
    return [code, generator.ORDER_ATOMIC];
};

export const basic_analog_t = basic_light;

export const basic_potentiometer = basic_light;


//////////////////////////显示屏///////////////////



///////////////////////////1位数码管/////////////////
export const basic_seg1 = function (_, generator) {
    var num = generator.valueToCode(this, 'num', generator.ORDER_ATOMIC);

    generator.definitions_['abc'] = 'int a=7,b=6,c=5,d=10,e=11,f=8,g=9,dp=4;';
    generator.definitions_['val3'] = 'int val=' + num + ';';

    generator.definitions_['digital_0'] = 'void digital_0(void)\n{\nunsigned char j;\ndigitalWrite(a,HIGH);\ndigitalWrite(b,HIGH);\ndigitalWrite(c,HIGH);\ndigitalWrite(d,HIGH);\ndigitalWrite(e,HIGH);\ndigitalWrite(f,HIGH);\ndigitalWrite(g,LOW);\ndigitalWrite(dp,LOW);\n}\n';

    generator.definitions_['digital_1'] = 'void digital_1(void)\n{\nunsigned char j;\ndigitalWrite(c,HIGH);\ndigitalWrite(b,HIGH);\nfor(j=7;j<=11;j++)\ndigitalWrite(j,LOW);\ndigitalWrite(dp,LOW);\n}\n';
    generator.definitions_['digital_2'] = 'void digital_2(void)\n{\nunsigned char j;\ndigitalWrite(b,HIGH);\ndigitalWrite(a,HIGH);\nfor(j=9;j<=11;j++)\ndigitalWrite(j,HIGH);\ndigitalWrite(dp,LOW);\ndigitalWrite(c,LOW);\ndigitalWrite(f,LOW);\n}\n';
    generator.definitions_['digital_3'] = 'void digital_3(void)\n{digitalWrite(g,HIGH);\ndigitalWrite(a,HIGH);\ndigitalWrite(b,HIGH);\ndigitalWrite(c,HIGH);\ndigitalWrite(d,HIGH);\ndigitalWrite(dp,LOW);\ndigitalWrite(f,LOW);\ndigitalWrite(e,LOW);\n}\n';
    generator.definitions_['digital_4'] = 'void digital_4(void) \n{digitalWrite(c,HIGH);\ndigitalWrite(b,HIGH);\ndigitalWrite(f,HIGH);\ndigitalWrite(g,HIGH);\ndigitalWrite(dp,LOW);\ndigitalWrite(a,LOW);\ndigitalWrite(e,LOW);\ndigitalWrite(d,LOW);\n}\n';
    generator.definitions_['digital_5'] = 'void digital_5(void)\n{\nunsigned char j;\ndigitalWrite(a,HIGH);\ndigitalWrite(b, LOW);\ndigitalWrite(c,HIGH);\ndigitalWrite(d,HIGH);\ndigitalWrite(e, LOW);\ndigitalWrite(f,HIGH);\ndigitalWrite(g,HIGH);\ndigitalWrite(dp,LOW);\n}\n';
    generator.definitions_['digital_6'] = 'void digital_6(void) \n{\nunsigned char j;\nfor(j=7;j<=11;j++)\ndigitalWrite(j,HIGH);\ndigitalWrite(c,HIGH);\ndigitalWrite(dp,LOW);\ndigitalWrite(b,LOW);\n}\n';
    generator.definitions_['digital_7'] = 'void digital_7(void)\n{\nunsigned char j;\nfor(j=5;j<=7;j++)\ndigitalWrite(j,HIGH);\ndigitalWrite(dp,LOW);\nfor(j=8;j<=11;j++)\ndigitalWrite(j,LOW);\n}\n';
    generator.definitions_['digital_8'] = 'void digital_8(void)\n{\nunsigned char j;\nfor(j=5;j<=11;j++)\ndigitalWrite(j,HIGH);\ndigitalWrite(dp,LOW);\n}\n';
    generator.definitions_['digital_9'] = 'void digital_9(void)\n{\nunsigned char j;\ndigitalWrite(a,HIGH);\ndigitalWrite(b,HIGH);\ndigitalWrite(c,HIGH);\ndigitalWrite(d,HIGH);\ndigitalWrite(e, LOW);\ndigitalWrite(f,HIGH);\ndigitalWrite(g,HIGH);\ndigitalWrite(dp,LOW);\n}\n';

    generator.setups_['setup_input'] = 'int i;\nfor(i=4;i<=11;i++)\npinMode(i,OUTPUT);\n';

    var code = 'switch(val)\n  {\n    case 0:digital_0();break;\n    case 1:digital_1();break;\n    case 2:digital_2();break;\n    case 3:digital_3();break;\n    case 4:digital_4();break;\n    case 5:digital_5();break;\n    case 6:digital_6();break;\n    case 7:digital_7();break;\n    case 8:digital_8();break;\n   case 9:digital_9();break;\n  }\n';
    return code;
};

///////////////////////////4位数码管/////////////////
export const basic_seg4 = function (_, generator) {
    var num = generator.valueToCode(this, 'num', generator.ORDER_ATOMIC);
    //var tc = generator.valueToCode(this, 'tc', generator.ORDER_ATOMIC);
    generator.definitions_['include_SevSeg-master'] = '#include "SevSeg.h"';
    generator.definitions_['sevseg'] = 'SevSeg sevseg;';
    generator.definitions_['val4'] = 'int val=' + num + ';';
    generator.definitions_['numDigits'] = 'byte numDigits = 4;';
    generator.definitions_['digitPins'] = 'byte digitPins[] = {2, 3, 4, 5};';
    generator.definitions_['segmentPins'] = 'byte segmentPins[] = {6, 7, 8, 9, 10, 11, 12, 13};';
    generator.definitions_['hardwareConfig'] = 'byte hardwareConfig = COMMON_CATHODE ; ';

    generator.setups_['setup_input'] = 'sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins);\n';

    var code = 'sevseg.setNumber(' + num + ', -1); \nsevseg.refreshDisplay();\n';
    return code;
};



////////////////////////1602LCD///////////////////////////
export const basic_1602lcd = function (_, generator) {
    var str1 = generator.valueToCode(this, 'TEXT1', generator.ORDER_ATOMIC) || 'String("")';
    var str2 = generator.valueToCode(this, 'TEXT2', generator.ORDER_ATOMIC) || 'String("")';


    generator.definitions_['DRE'] = 'int DI = 12,RW = 11,Enable = 2;';
    generator.definitions_['DB'] = 'int DB[] = {3, 4, 5, 6, 7, 8, 9, 10};';
    generator.definitions_['STR'] = 'char *string = "0";';
    generator.definitions_['LcdCommandWrite'] = 'void LcdCommandWrite(int value) \n{\nint i = 0;\nfor (i=DB[0]; i <= DI; i++) \n{\ndigitalWrite(i,value & 01);\nvalue >>= 1;\n}\ndigitalWrite(Enable,LOW);\ndelayMicroseconds(1);\ndigitalWrite(Enable,HIGH);\ndelayMicroseconds(1); \ndigitalWrite(Enable,LOW);\ndelayMicroseconds(1); \n}\n';
    generator.definitions_['LcdDataWrite'] = 'void LcdDataWrite(int value) \n{\nint i = 0;\ndigitalWrite(DI, HIGH); \ndigitalWrite(RW, LOW); \nfor (i=DB[0]; i <= DB[7]; i++) \n{\ndigitalWrite(i,value & 01);\nvalue >>= 1;\n}\ndigitalWrite(Enable,LOW);  \ndelayMicroseconds(1);\ndigitalWrite(Enable,HIGH); \ndelayMicroseconds(1);\ndigitalWrite(Enable,LOW); \ndelayMicroseconds(1); \n}\n';
    //generator.definitions_['LcdStringWrite'] = 'void LcdStringWrite(char *a)\n{\nfor(int i = 0;i < 14; i++)\n{\nif(*(a+i) == '\0')\n {\nbreak;\n}\nLcdDataWrite(*(a+i));\n}\n}\n';
    generator.definitions_['lsw'] = 'void LcdStringWrite(char *a)\n{\nfor(int i = 0;i < strlen(a); i++){\nLcdDataWrite(*(a+i));\n}\n}\n';

    generator.setups_['setup_lcd'] = 'int i = 0;\nfor (i=Enable; i <= DI; i++) \n{\n    pinMode(i,OUTPUT);\n}\ndelay(100);\nLcdCommandWrite(0x38);\ndelay(64);  \nLcdCommandWrite(0x38); \ndelay(50);  \nLcdCommandWrite(0x38); \ndelay(20);  \nLcdCommandWrite(0x06); \ndelay(20);  \nLcdCommandWrite(0x0E);\ndelay(20);  \nLcdCommandWrite(0x01);  \ndelay(100); \nLcdCommandWrite(0x80); \ndelay(20);  \n';

    var code = 'LcdCommandWrite(0x01); \ndelay(10); \nLcdCommandWrite(0x80+0); \ndelay(10); \nstring = ' + str1 + ';\nLcdStringWrite(string);\ndelay(10);\nLcdCommandWrite(0xc0+0); \n string = ' + str2 + ';\nLcdStringWrite(string);\ndelay(10);\ndelay(300);';

    return code;
};

////////////////////////////////点阵////////////////////////////
export const basic_matrix_init = function (_, generator) {
    generator.definitions_['matrix_var'] = 'unsigned char data_clear[]={0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};\n' +
        'unsigned char data_0[]={0x00,0x38,0x44,0x44,0x44,0x44,0x38,0x00};\n' +
        'unsigned char data_1[]={0x38,0x10,0x10,0x10,0x10,0x10,0x30,0x10};\n' +
        'unsigned char data_2[]={0x7c,0x20,0x10,0x08,0x04,0x44,0x38,0x00};\n' +
        'unsigned char data_3[]={0x38,0x44,0x04,0x38,0x04,0x44,0x38,0x00};\n' +
        'unsigned char data_4[]={0x08,0x08,0x08,0x7e,0x48,0x28,0x10,0x08};\n' +
        'unsigned char data_5[]={0x00,0x78,0x04,0x04,0x78,0x40,0x7c,0x40};\n' +
        'unsigned char data_6[]={0x00,0x38,0x44,0x44,0x78,0x20,0x10,0x0c};\n' +
        'unsigned char data_7[]={0x00,0x20,0x20,0x10,0x08,0x04,0x7e,0x00};\n' +
        'unsigned char data_8[]={0x38,0x44,0x44,0x38,0x44,0x44,0x38,0x00};\n' +
        'unsigned char data_9[]={0x38,0x44,0x04,0x3c,0x44,0x44,0x38,0x00};';

    generator.definitions_['DC'] = 'void clear_(void)\n' +
        '{\n' +
        '  for(int i=2;i<10;i++)\n' +
        '  digitalWrite(i, LOW);\n' +
        '  for(int i=0;i<8;i++)\n' +
        '  digitalWrite(i+10, HIGH);\n' +
        '}\n';

    generator.definitions_['DP'] = 'void Draw_point(unsigned char x,unsigned char y)\n' +
        '{\n' +
        '  clear_();\n' +
        '  digitalWrite(x+1, HIGH);\n' +
        '  digitalWrite(y+10, LOW);\n' +
        '  delay(1);\n' +
        '}\n';

    generator.definitions_['SN'] = 'void show_num(unsigned char matrix_data[])\n' +
        '{\n' +
        '  unsigned char i,j,data;\n' +
        '  for(i=8;i>0;i--)\n' +
        '  {\n' +
        '    data=matrix_data[i];\n' +
        '    for(j=8;j>0;j--)\n' +
        '    {\n' +
        '      if(data & 0x01)Draw_point(j,i);\n' +
        '      data>>=1;\n' +
        '    }\n' +
        '  }\n' +
        '}\n';

    generator.setups_['setup_matrix'] = 'for(int i=2;i<18;i++)\n' +
        '  {\n' +
        '    pinMode(i, OUTPUT);\n' +
        '  }\n' +
        '  clear_();\n';

    return '';
};

export const basic_matrix8 = function (_, generator) {
    var matrix_data = generator.valueToCode(this, 'num', generator.ORDER_ATOMIC);

    return 'show_num(data_' + matrix_data + ');';
};

////////////////////////////////点阵1////////////////////////////
//执行器_点阵屏显示_显示图案
export const basic_matrix1 = function (_, generator) {
    var dotMatrixArray = generator.valueToCode(this, 'LEDArray', generator.ORDER_ASSIGNMENT);

    return 'show_num(data_' + dotMatrixArray + ');';
};
//执行器_点阵屏显示_点阵数组
export const basic_matrix2 = function (_, generator) {
    var varName = this.getFieldValue('VAR');
    var a = new Array();
    for (let i = 1; i < 9; i++) {
        a[i] = new Array();
        for (let j = 1; j < 9; j++) {
            a[i][j] = (this.getFieldValue('a' + i + j) == "TRUE") ? 1 : 0;
        }
    }
    var code = '{';
    for (let i = 1; i < 9; i++) {
        var tmp = ""
        for (let j = 1; j < 9; j++) {
            tmp += a[i][j];
        }
        tmp = (parseInt(tmp, 2)).toString(16)
        if (tmp.length == 1) tmp = "0" + tmp;
        code += '0x' + tmp + ((i != 8) ? ',' : '');
    }
    code += '};';
    //generator.definitions_[this.id] = "byte LedArray_"+clearString(this.id)+"[]="+code;
    generator.definitions_[varName] = "unsigned char data_" + varName + "[8]=" + code;
    //return ["LedArray_"+clearString(this.id), generator.ORDER_ATOMIC];
    return [varName, generator.ORDER_ATOMIC];
};

export const basic_matrix_clear = function () {
    return 'clear_();';
};