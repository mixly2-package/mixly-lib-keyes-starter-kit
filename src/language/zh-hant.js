export const ZhHantMsg = {};

ZhHantMsg.MIXLY_basic_LED1 = '蓝色LED';
ZhHantMsg.MIXLY_basic_LED2 = '红色LED';
ZhHantMsg.MIXLY_basic_LED3 = '黄色LED';
ZhHantMsg.MIXLY_basic_LED4 = '绿色LED';
ZhHantMsg.MIXLY_basic_analog = '模拟输出';
ZhHantMsg.MIXLY_basic_LED4 = '黃色食人魚LED';
ZhHantMsg.MIXLY_basic_LED5 = '藍色食人魚LED';
ZhHantMsg.MIXLY_basic_LED01 = '草帽LED';
ZhHantMsg.MIXLY_basic_LED02 = '紅色草帽LED';
ZhHantMsg.MIXLY_basic_LED03 = '綠色草帽LED';
ZhHantMsg.MIXLY_basic_LED04 = '黃色草帽LED';
ZhHantMsg.MIXLY_basic_LED05 = '藍色草帽LED';
ZhHantMsg.MIXLY_basic_BUZZER1 = '有源蜂鳴器';
ZhHantMsg.MIXLY_basic_BUZZER2 = '無源蜂鳴器';
ZhHantMsg.MIXLY_basic_RELAY = '繼電器';
ZhHantMsg.MIXLY_basic_MOTOR = '電機';
ZhHantMsg.MIXLY_basic_SERVO = '舵機';
ZhHantMsg.MIXLY_basic_2812RGB = '2812RGB模塊';

ZhHantMsg.MIXLY_basic_IR_G = '人體紅外熱模塊';
ZhHantMsg.MIXLY_basic_FLAME = '火焰傳感器';
ZhHantMsg.MIXLY_basic_HALL = '霍爾傳感器';
ZhHantMsg.MIXLY_basic_CRASH = '碰撞傳感器';
ZhHantMsg.MIXLY_basic_BUTTON = '按鍵';
ZhHantMsg.MIXLY_basic_TUOCH = '電容觸摸';
ZhHantMsg.MIXLY_basic_KNOCK = '敲擊模塊';
ZhHantMsg.MIXLY_basic_TILT = '傾斜模塊';
ZhHantMsg.MIXLY_basic_SHAKE = '振動模塊';
ZhHantMsg.MIXLY_basic_REED_S = '幹簧管模塊';
ZhHantMsg.MIXLY_basic_TRACK = '循跡模塊';
ZhHantMsg.MIXLY_basic_AVOID = '避障模塊';
ZhHantMsg.MIXLY_basic_LIGHT_B = '光折斷模塊';

ZhHantMsg.MIXLY_basic_ANALOG_T = '模擬溫度傳感器';
ZhHantMsg.MIXLY_basic_SOUND = '聲音傳感器';
ZhHantMsg.MIXLY_basic_LIGHT = '光線傳感器';
ZhHantMsg.MIXLY_basic_WATER = '水位傳感器';
ZhHantMsg.MIXLY_basic_SOIL = '土壤傳感器';
ZhHantMsg.MIXLY_basic_POTENTIOMETER = '電位器';
ZhHantMsg.MIXLY_basic_LM35 = 'LM35溫度傳感器';
ZhHantMsg.MIXLY_basic_SLIDE_POTENTIOMETER = '滑動電位器';
ZhHantMsg.MIXLY_basic_TEMT6000 = 'TEMT6000環境光';
ZhHantMsg.MIXLY_basic_STEAM = '水蒸氣傳感器';
ZhHantMsg.MIXLY_basic_FILM_P = '薄膜壓力傳感器';
ZhHantMsg.MIXLY_basic_JOYSTICK = '遙桿模塊';
ZhHantMsg.MIXLY_basic_SMOKE = '煙霧傳感器';
ZhHantMsg.MIXLY_basic_ALCOHOL = '酒精傳感器';
ZhHantMsg.MIXLY_basic_MQ135 = 'MQ135空氣質量';
ZhHantMsg.MIXLY_basic_18B20 = '18B20溫度模塊';
ZhHantMsg.MIXLY_basic_RT = '获取温度';

ZhHantMsg.MIXLY_basic_DHT11 = '溫濕度模塊';
ZhHantMsg.MIXLY_basic_BMP180 = 'BMP180高度計模塊';
ZhHantMsg.MIXLY_basic_T = '温度';
ZhHantMsg.MIXLY_basic_QY = '大气压';
ZhHantMsg.MIXLY_basic_H = '高度';

ZhHantMsg.MIXLY_basic_SR01 = 'SR01超聲波模塊';
ZhHantMsg.MIXLY_basic_3231 = '3231時鐘';
ZhHantMsg.MIXLY_basic_ADXL345 = '加速度模塊';

ZhHantMsg.MIXLY_basic_CARD1 = '卡1';
ZhHantMsg.MIXLY_basic_CARD2 = '卡2';

ZhHantMsg.MIXLY_basic_16button = '4*4按钮键盘';


ZhHantMsg.MIXLY_basic_OLED = 'OLED模塊';
ZhHantMsg.MIXLY_basic_1602LCD = 'IIC1602LCD';
ZhHantMsg.MIXLY_basic_2004LCD = 'IIC2004LCD';
ZhHantMsg.MIXLY_basic_MATRIX = '8*8點陣';

ZhHantMsg.MIXLY_basic_TM1637 = '4位8段數碼管';
ZhHantMsg.MIXLY_basic_SMG = '1位8段数码管';
ZhHantMsg.MIXLY_basic_ws = '位数';
ZhHantMsg.MIXLY_basic_value = '数值';
ZhHantMsg.MIXLY_basic_begin = '显示的位置';
ZhHantMsg.MIXLY_basic_fill0 = '是否补充0';
ZhHantMsg.MIXLY_basic_light = '亮度0~7';
ZhHantMsg.MIXLY_basic_XY = '显或隐';
ZhHantMsg.MIXLY_basic_L = '左边';
ZhHantMsg.MIXLY_basic_R = '右边';
ZhHantMsg.MIXLY_basic_MH = '冒号';
ZhHantMsg.MIXLY_basic_one = '第一行';
ZhHantMsg.MIXLY_basic_two = '第二行';
ZhHantMsg.MIXLY_basic_three = '第三行';
ZhHantMsg.MIXLY_basic_four = '第四行';


ZhHantMsg.MIXLY_basic_IR_E = '紅外發射模塊';
ZhHantMsg.MIXLY_basic_IR_R = '紅外接收模塊';
ZhHantMsg.MIXLY_basic_W5100 = 'W5100以太網模塊';
ZhHantMsg.MIXLY_basic_BLUETOOTH = '藍牙2.0模塊';
ZhHantMsg.MIXLY_basic_rec = '接收到的信号';

ZhHantMsg.MIXLY_basic_Count = '灯号';

ZhHantMsg.MIXLY_basic_Matrix_init = '8*8点阵初始化';
ZhHantMsg.MIXLY_basic_Matrix_custom = '点阵自定义显示';
ZhHantMsg.MIXLY_basic_Matrix_char = '8*8点阵显示单个字符';
ZhHantMsg.MIXLY_basic_Matrix_dis = '显示';

export const ZhHantCatgories = {};