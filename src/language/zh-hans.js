export const ZhHansMsg = {};

ZhHansMsg.MIXLY_basic_LED1 = '蓝色LED';
ZhHansMsg.MIXLY_basic_LED2 = '红色LED';
ZhHansMsg.MIXLY_basic_LED3 = '黄色LED';
ZhHansMsg.MIXLY_basic_LED4 = '绿色LED';
ZhHansMsg.MIXLY_basic_analog = '模拟输出';
ZhHansMsg.MIXLY_basic_LED01 = '草帽LED';
ZhHansMsg.MIXLY_basic_LED02 = '红色草帽LED';
ZhHansMsg.MIXLY_basic_LED03 = '绿色草帽LED';
ZhHansMsg.MIXLY_basic_LED04 = '黄色草帽LED';
ZhHansMsg.MIXLY_basic_LED05 = '蓝色草帽LED';
ZhHansMsg.MIXLY_basic_BUZZER1 = '有源蜂鸣器';
ZhHansMsg.MIXLY_basic_BUZZER2 = '无源蜂鸣器';
ZhHansMsg.MIXLY_basic_RELAY = '继电器';
ZhHansMsg.MIXLY_basic_MOTOR = '电机风扇';
ZhHansMsg.MIXLY_basic_SERVO = '舵机';
ZhHansMsg.MIXLY_basic_2812RGB = '2812RGB模块';

ZhHansMsg.MIXLY_basic_IR_G = '人体红外热模块';
ZhHansMsg.MIXLY_basic_FLAME = '火焰传感器';
ZhHansMsg.MIXLY_basic_HALL = '霍尔传感器';
ZhHansMsg.MIXLY_basic_CRASH = '碰撞传感器';
ZhHansMsg.MIXLY_basic_BUTTON = '按键';
ZhHansMsg.MIXLY_basic_TUOCH = '电容触摸';
ZhHansMsg.MIXLY_basic_KNOCK = '敲击模块';
ZhHansMsg.MIXLY_basic_TILT = '倾斜模块';
ZhHansMsg.MIXLY_basic_SHAKE = '振动模块';
ZhHansMsg.MIXLY_basic_REED_S = '干簧管模块';
ZhHansMsg.MIXLY_basic_TRACK = '循迹模块';
ZhHansMsg.MIXLY_basic_AVOID = '避障模块';
ZhHansMsg.MIXLY_basic_LIGHT_B = '光折断模块';

ZhHansMsg.MIXLY_basic_ANALOG_T = '模拟温度传感器';
ZhHansMsg.MIXLY_basic_SOUND = '声音传感器';
ZhHansMsg.MIXLY_basic_LIGHT = '光线传感器';
ZhHansMsg.MIXLY_basic_WATER = '水位传感器';
ZhHansMsg.MIXLY_basic_SOIL = '土壤传感器';
ZhHansMsg.MIXLY_basic_POTENTIOMETER = '电位器';
ZhHansMsg.MIXLY_basic_LM35 = 'LM35温度传感器';
ZhHansMsg.MIXLY_basic_SLIDE_POTENTIOMETER = '滑动电位器';
ZhHansMsg.MIXLY_basic_TEMT6000 = 'TEMT6000环境光';
ZhHansMsg.MIXLY_basic_STEAM = '水蒸气传感器';
ZhHansMsg.MIXLY_basic_FILM_P = '薄膜压力传感器';
ZhHansMsg.MIXLY_basic_JOYSTICK = '遥杆模块';
ZhHansMsg.MIXLY_basic_SMOKE = '烟雾传感器';
ZhHansMsg.MIXLY_basic_ALCOHOL = '酒精传感器';
ZhHansMsg.MIXLY_basic_MQ135 = 'MQ135空气质量';

ZhHansMsg.MIXLY_basic_18B20 = '18B20温度模块';
ZhHansMsg.MIXLY_basic_RT = '温度';

ZhHansMsg.MIXLY_basic_DHT11 = '温湿度模块';
ZhHansMsg.MIXLY_basic_BMP180 = 'BMP180高度计模块';
ZhHansMsg.MIXLY_basic_T = '温度';
ZhHansMsg.MIXLY_basic_QY = '大气压';
ZhHansMsg.MIXLY_basic_H = '高度';

ZhHansMsg.MIXLY_basic_SR01 = 'SR01超声波模块';
ZhHansMsg.MIXLY_basic_3231 = 'DS3231时钟';
ZhHansMsg.MIXLY_basic_ADXL345 = '加速度模块';

ZhHansMsg.MIXLY_basic_YEAR = '年';
ZhHansMsg.MIXLY_basic_MONTH = '月';
ZhHansMsg.MIXLY_basic_DAY = '天';
ZhHansMsg.MIXLY_basic_TEXT = '周';
ZhHansMsg.MIXLY_basic_HOUR = '时';
ZhHansMsg.MIXLY_basic_MINUTE = '分';
ZhHansMsg.MIXLY_basic_SECOND = '秒';
ZhHansMsg.MIXLY_basic_GET = '获取时间';

ZhHansMsg.MIXLY_basic_CARD1 = '卡1';
ZhHansMsg.MIXLY_basic_CARD2 = '卡2';

ZhHansMsg.MIXLY_basic_16button = '4*4按钮键盘';


ZhHansMsg.MIXLY_basic_OLED = 'OLED模块';
ZhHansMsg.MIXLY_basic_1602LCD = 'IIC1602LCD';
ZhHansMsg.MIXLY_basic_2004LCD = 'IIC2004LCD';
ZhHansMsg.MIXLY_basic_MATRIX = '8*8点阵';
ZhHansMsg.MIXLY_basic_TM1637 = '4位8段数码管';
ZhHansMsg.MIXLY_basic_SMG = '1位8段数码管';
ZhHansMsg.MIXLY_basic_ws = '位数';
ZhHansMsg.MIXLY_basic_begin = '显示的位置';
ZhHansMsg.MIXLY_basic_fill0 = '是否补充0';
ZhHansMsg.MIXLY_basic_light = '亮度0~7';
ZhHansMsg.MIXLY_basic_XY = '显或隐';
ZhHansMsg.MIXLY_basic_L = '左边';
ZhHansMsg.MIXLY_basic_R = '右边';
ZhHansMsg.MIXLY_basic_MH = '冒号';
ZhHansMsg.MIXLY_basic_one = '第一行';
ZhHansMsg.MIXLY_basic_two = '第二行';
ZhHansMsg.MIXLY_basic_three = '第三行';
ZhHansMsg.MIXLY_basic_four = '第四行';

ZhHansMsg.MIXLY_basic_value = '数值';

ZhHansMsg.MIXLY_basic_IR_E = '红外发射模块';
ZhHansMsg.MIXLY_basic_IR_R = '红外接收模块';
ZhHansMsg.MIXLY_basic_W5100 = 'W5100以太网模块';
ZhHansMsg.MIXLY_basic_BLUETOOTH = '蓝牙2.0模块';
ZhHansMsg.MIXLY_basic_rec = '接收到的信号';

ZhHansMsg.MIXLY_basic_kzsc = '控制输出';

ZhHansMsg.MIXLY_basic_Count = '灯号';

ZhHansMsg.MIXLY_basic_Matrix_init = '8*8点阵初始化';
ZhHansMsg.MIXLY_basic_Matrix_custom = '8*8点阵自定义显示';
ZhHansMsg.MIXLY_basic_Matrix_char = '8*8点阵显示单个字符';
ZhHansMsg.MIXLY_basic_Matrix_dis = '显示';

export const ZhHansCatgories = {};