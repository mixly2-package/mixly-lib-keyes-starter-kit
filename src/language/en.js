export const EnMsg = {};

EnMsg.MIXLY_basic_LED1 = 'Blue_LED';
EnMsg.MIXLY_basic_LED2 = 'Red_LED';
EnMsg.MIXLY_basic_LED3 = 'Yellow_LED';
EnMsg.MIXLY_basic_LED4 = 'Green_LED';
EnMsg.MIXLY_basic_analog = 'analogWrite';
EnMsg.MIXLY_basic_LED4 = 'Yellow_LED';
EnMsg.MIXLY_basic_LED5 = 'Blue Piranha LED';
EnMsg.MIXLY_basic_LED01 = 'Straw cap LED';
EnMsg.MIXLY_basic_LED02 = 'Red Straw cap LED';
EnMsg.MIXLY_basic_LED03 = 'Green Straw cap LED';
EnMsg.MIXLY_basic_LED04 = 'Yellow straw cap LED';
EnMsg.MIXLY_basic_LED05 = 'Blue Straw cap LED';
EnMsg.MIXLY_basic_BUZZER1 = 'Active buzzer';
EnMsg.MIXLY_basic_BUZZER2 = 'Passive Buzzer';
EnMsg.MIXLY_basic_RELAY = 'Relay';
EnMsg.MIXLY_basic_MOTOR = 'Motor';
EnMsg.MIXLY_basic_SERVO = 'servo';
EnMsg.MIXLY_basic_2812RGB = '2812RGB Module';

EnMsg.MIXLY_basic_IR_G = 'PIR Module';
EnMsg.MIXLY_basic_FLAME = 'Flame Sensor';
EnMsg.MIXLY_basic_HALL = 'Hall Sensor';
EnMsg.MIXLY_basic_CRASH = 'Crash Sensor';
EnMsg.MIXLY_basic_BUTTON = 'Button switch';
EnMsg.MIXLY_basic_TUOCH = 'Capacitive Touch';
EnMsg.MIXLY_basic_KNOCK = 'Knock Module';
EnMsg.MIXLY_basic_TILT = 'Tilt switch';
EnMsg.MIXLY_basic_SHAKE = 'Vibration Module';
EnMsg.MIXLY_basic_REED_S = 'Reed Switch Module';
EnMsg.MIXLY_basic_TRACK = 'Tracking Module';
EnMsg.MIXLY_basic_AVOID = 'Obstacle Avoidance Module';
EnMsg.MIXLY_basic_LIGHT_B = 'Light Interrupt Module';

EnMsg.MIXLY_basic_ANALOG_T = 'Analog Temperature Sensor';
EnMsg.MIXLY_basic_SOUND = 'Sound Sensor';
EnMsg.MIXLY_basic_LIGHT = 'Light Sensor';
EnMsg.MIXLY_basic_WATER = 'Water Level Sensor';
EnMsg.MIXLY_basic_SOIL = 'Soil Sensor';
EnMsg.MIXLY_basic_POTENTIOMETER = 'potentiometer';
EnMsg.MIXLY_basic_LM35 = 'LM35 Temperature Sensor';
EnMsg.MIXLY_basic_SLIDE_POTENTIOMETER = 'slide potentiometer';
EnMsg.MIXLY_basic_TEMT6000 = 'TEMT6000 Ambient Light';
EnMsg.MIXLY_basic_STEAM = 'water vapor sensor';
EnMsg.MIXLY_basic_FILM_P = 'Thin-film Pressure Sensor';
EnMsg.MIXLY_basic_JOYSTICK = 'Joystick Module';
EnMsg.MIXLY_basic_SMOKE = 'Smoke Sensor';
EnMsg.MIXLY_basic_ALCOHOL = 'Alcohol Sensor';
EnMsg.MIXLY_basic_MQ135 = 'MQ135 Air Quality';
EnMsg.MIXLY_basic_18B20 = '18B20 Temperature Module';
EnMsg.MIXLY_basic_RT = 'temperature';

EnMsg.MIXLY_basic_DHT11 = 'temperature and humidity module';
EnMsg.MIXLY_basic_BMP180 = 'BMP180 altimeter module';
EnMsg.MIXLY_basic_T = 'temperature';
EnMsg.MIXLY_basic_QY = 'barometric pressure';
EnMsg.MIXLY_basic_H = 'altitude';

EnMsg.MIXLY_basic_SR01 = 'SR01 Ultrasound Module';
EnMsg.MIXLY_basic_3231 = 'DS3231 clock';
EnMsg.MIXLY_basic_ADXL345 = 'Acceleration Module';

EnMsg.MIXLY_basic_CARD1 = 'card1';
EnMsg.MIXLY_basic_CARD2 = 'card2';

EnMsg.MIXLY_basic_16button = '4*4button';


EnMsg.MIXLY_basic_OLED = 'OLED Module';
EnMsg.MIXLY_basic_1602LCD = 'IIC1602LCD';
EnMsg.MIXLY_basic_2004LCD = 'IIC2004LCD';
EnMsg.MIXLY_basic_MATRIX = '8*8 dot matrix';
EnMsg.MIXLY_basic_TM1637 = '4 digit 8-segment LED digital';
EnMsg.MIXLY_basic_SMG = '1 digit 8-segment LED digital';
EnMsg.MIXLY_basic_ws = 'digit';
EnMsg.MIXLY_basic_begin = 'Display position';
EnMsg.MIXLY_basic_fill0 = 'add 0?';
EnMsg.MIXLY_basic_light = 'Brightness0~7';
EnMsg.MIXLY_basic_XY = 'Show or hide';
EnMsg.MIXLY_basic_L = 'left';
EnMsg.MIXLY_basic_R = 'right';
EnMsg.MIXLY_basic_MH = 'colon';
EnMsg.MIXLY_basic_one = 'print line1';
EnMsg.MIXLY_basic_two = 'print line2';
EnMsg.MIXLY_basic_three = 'print line3';
EnMsg.MIXLY_basic_four = 'print line4';


EnMsg.MIXLY_basic_value = 'value';


EnMsg.MIXLY_basic_IR_E = 'Infrared Transmitter Module';
EnMsg.MIXLY_basic_IR_R = 'Infrared Receiver Module';
EnMsg.MIXLY_basic_W5100 = 'W5100 Ethernet Module';
EnMsg.MIXLY_basic_BLUETOOTH = 'Bluetooth 2.0 Module';
EnMsg.MIXLY_basic_rec = 'Received';


//EnMsg.MIXLY_basic_kzsc = 'Control output';

EnMsg.MIXLY_basic_Count = 'count';

EnMsg.MIXLY_basic_Matrix_init = 'Matrix 8*8 Init';
EnMsg.MIXLY_basic_Matrix_custom = 'Matrix custom display';
EnMsg.MIXLY_basic_Matrix_char = 'Matrix Display a single character';
EnMsg.MIXLY_basic_Matrix_dis = 'display';

export const EnCatgories = {};